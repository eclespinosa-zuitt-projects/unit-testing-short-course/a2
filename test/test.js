const { assert } = require("chai");
const { checkAge, checkFullName } = require("./../src/util");

describe("test_checkAge", () => {
  it("age_an_integer", () => {
    const age = 12.5;
    assert.strictEqual(checkAge(age) % 1, NaN);
  });

  it("age_an_integer", () => {
    const age = 12;
    assert.strictEqual(checkAge(age) % 1, 0);
  });

  it("age_not_empty", () => {
    const age = "";
    assert.isNotEmpty(checkAge(age));
  });

  it("age_not_undefined", () => {
    const age = undefined;
    assert.isDefined(checkAge(age));
  });

  it("age_not_null", () => {
    const age = null;
    assert.isNotNull(checkAge(age));
  });

  it("age_not_zero", () => {
    const age = 0;
    assert.notEqual(checkAge(age), 0);
  });
});

describe("test_checkFullName", () => {
  it("name_not_empty", () => {
    const name = "";
    assert.isNotEmpty(checkFullName(name));
  });

  it("name_not_null", () => {
    const name = null;
    assert.isNotNull(checkFullName(name));
  });

  it("name_not_undefined", () => {
    const name = undefined;
    assert.isDefined(checkFullName(name));
  });

  it("name_type_of_string", () => {
    const name = 54;
    assert.isString(checkFullName(name));
  });

  it("name_character_not_zero", () => {
    const name = "";
    assert.isNotEmpty(checkFullName(name));
  });
});
