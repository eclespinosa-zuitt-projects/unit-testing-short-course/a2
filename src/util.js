function checkAge(age) {
  if (age === undefined) return "Error: Age should not be undefined";
  if (age === null) return "Error: Age should not be null";
  if (age === "") return "Error: Age should not be empty";
  if (age % 1 !== 0) throw Error("Age should be whole number");
  if (age <= 0) return "Error: Age should not be greater than zero";

  return age;
}

function checkFullName(fullName) {
  if (fullName === "") return "Error: Name should not be empty";
  if (fullName === undefined) return "Error: Name should not be undefined";
  if (fullName === null) return "Error: Name should not be null";
  if (typeof fullName !== "string") return "Error: Name should be string";
  if (fullName.length === 0)
    return "Error: Name characters should be greater than 0";

  return fullName;
}

module.exports = {
  checkAge,
  checkFullName,
};
